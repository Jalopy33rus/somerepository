<<<<<<< HEAD
11111

=======
a'll be back
>>>>>>> dda6acacd1abcbfa18d486c779ac5a461aa943f9
library IEEE;
use     IEEE.STD_LOGIC_1164.all;
use     IEEE.STD_LOGIC_UNSIGNED.all;
use     IEEE.STD_LOGIC_ARITH.all;


entity fir_filter is
    port(
        CLK        : in  std_logic;
        DATA_IN    : in  std_logic_vector( 7 downto 0) := (  others => '0');
        DVI        : in  std_logic;
   
        DVO        : out  std_logic;
        DATA_OUT   : out  std_logic_vector( 7 downto 0) := (  others => '0')
    );
end fir_filter;

5645645456465465

architecture fir_filter_arch of fir_filter is
    type Coeficient_type is array (2 downto 0) of std_logic_vector (17 downto 0);  
    constant coef: coeficient_type :=   
                               (    X"F1",  
                                    X"42",  
                                    X"F1"                                     
                                    );
     signal sum: std_logic_vector(25 downto 0); 
     signal delay0 : std_logic_vector(7 downto 0);  
     signal delay1 : std_logic_vector(7 downto 0);  
     signal dv_delay0 : std_logic;  

     signal mult0 : std_logic_vector(25 downto 0);  
     signal mult1 : std_logic_vector(25 downto 0);  
     signal mult2 : std_logic_vector(25 downto 0);  


begin

    process (CLK) begin

    if CLK'event and CLK = '1' then
        dv_delay0 <= DVI;
        DVO <= dv_delay0;
        
        if DVI = '1' then
            delay0 <= DATA_IN;
            delay1 <= delay0;
            
            mult0 <= signed(DATA_IN) * signed(coef(0));
            mult1 <= signed(delay0) * signed(coef(1));
            mult2 <= signed(delay1) * signed(coef(2));
            
            if dv_delay0 = '1' then
                sum <= mult0 + mult1 + mult2;
            end if;
        end if;
        
        DATA_OUT <= sum (25 downto 18);
    end if;
    end process;
end fir_filter_arch;